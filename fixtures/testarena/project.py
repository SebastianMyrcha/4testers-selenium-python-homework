import random
import string
import pytest


@pytest.fixture
def project():
    name = "SEBA"
    project = generate_random_project(name, length=8)
    yield project


def generate_random_project(name, length=8):
    characters = string.ascii_uppercase
    random_string = ''.join(random.choice(characters) for _ in range(length))
    project_name = f'{name} {random_string}'
    return {
        "name": project_name,
        "prefix": f'{name[0:2]}{random_string[0:5]}',
        "description": f'Super project {project_name}'
    }


