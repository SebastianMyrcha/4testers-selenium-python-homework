from selenium.common import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait


class ProjectViewPage:
    success_info_box = (By.CSS_SELECTOR, '#j_info_box p')
    project_name_element = (By.CLASS_NAME, 'content_label_title')
    success_info_text = 'Projekt został dodany.'
    projects_menu_link = (By.CLASS_NAME, 'activeMenu')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def verify_project_added(self, name):
        try:
            self.wait.until(lambda x: self.success_info_text in self.browser.find_element(*self.success_info_box).text)
        except TimeoutException:
            print(
                f"Error: The expected text '{self.success_info_text}' "
                f"was not found in any message_content_text element within the given time.")

        try:
            self.wait.until(lambda x: name in self.browser.find_element(*self.project_name_element).text)
        except TimeoutException:
            print(
                f"Error: The expected text '{name}' "
                f"was not found in any message_content_text element within the given time.")
        return self

    def click_projects_menu_link(self):
        self.browser.find_element(*self.projects_menu_link).click()
