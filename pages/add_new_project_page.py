from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from pages.project_view_page import ProjectViewPage


class AddNewProjectPage:
    name_input = (By.CSS_SELECTOR, "#name")
    prefix_input = (By.CSS_SELECTOR, '#prefix')
    description_textarea = (By.CSS_SELECTOR, '#description')
    save_button = (By.CSS_SELECTOR, '#save')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def add_new_project(self, name, prefix, description):
        self.wait.until(ec.element_to_be_clickable(self.name_input))
        self.browser.find_element(*self.name_input).send_keys(name)
        self.browser.find_element(*self.prefix_input).send_keys(prefix)
        self.browser.find_element(*self.description_textarea).send_keys(description)
        self.browser.find_element(*self.save_button).click()
        return ProjectViewPage(self.browser)
