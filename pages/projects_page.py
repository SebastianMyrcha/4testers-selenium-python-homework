from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait

from pages.add_new_project_page import AddNewProjectPage


class ProjectsPage:
    add_project_button = (By.CSS_SELECTOR, '[href="http://demo.testarena.pl/administration/add_project"]')
    mail_icon = (By.CLASS_NAME, 'icon_mail')
    search_input = (By.CSS_SELECTOR, '#search')
    search_button = (By.CSS_SELECTOR, '#j_searchButton')
    project_found_link = (By.CSS_SELECTOR, 'table td:nth-child(1) > a')

    def __init__(self, browser):
        self.browser = browser
        self.wait = WebDriverWait(browser, 10)

    def click_add_project(self):
        self.browser.find_element(*self.add_project_button).click()
        return AddNewProjectPage(self.browser)

    def search_project(self, name):
        self.wait.until(ec.element_to_be_clickable(self.search_input))
        self.browser.find_element(*self.search_input).send_keys(name)
        self.browser.find_element(*self.search_button).click()
        return self

    def verify_project_found(self, expected_project_name):
        self.wait.until(ec.element_to_be_clickable(self.project_found_link))
        project_name = self.browser.find_element(*self.project_found_link).text
        assert expected_project_name in project_name
