from fixtures.chrome import chrome_browser
from fixtures.testarena.login import browser
from fixtures.testarena.project import project
from pages.home_page import HomePage
from pages.projects_page import ProjectsPage


def test_add_project(browser, project):
    home_page = HomePage(browser)
    home_page.click_admin_icon()

    projects_page = ProjectsPage(browser)
    (projects_page
     .click_add_project()
     .add_new_project(project['name'], project['prefix'], project['description'])
     .verify_project_added(project['name'])
     .click_projects_menu_link())

    projects_page = ProjectsPage(browser)
    (projects_page
     .search_project(project['name'])
     .verify_project_found(project['name']))
